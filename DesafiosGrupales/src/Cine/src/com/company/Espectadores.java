package com.company;

public class Espectadores extends Persona{
    private String fila;
    private int silla;
    private String tipo = "Espectador";

    public Espectadores(String nombre, int edad, String fila, int silla) {
        super.setNombre(nombre);
        super.setEdad(edad);
        this.fila = fila;
        this.silla = silla;
    }

    @Override
    public String getTipo() {
        return "Tipo= " + tipo;
    }
    public String getButaca(){
        return "Butaca= "+ fila+silla;
    }
}
