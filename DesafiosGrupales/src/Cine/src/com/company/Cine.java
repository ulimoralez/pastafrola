package com.company;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class Cine {

    public static void main(String[] args) {

        Set<Espectadores> espectadores = new HashSet<Espectadores>();
        int seguir = 1;

        Scanner sc = new Scanner(System.in);


        System.out.println("**** Cree una sala ****");

        System.out.print("Nombre: ");
        String nombreSala = sc.next();

        System.out.print("capacidad: ");
        int capacidadSala = sc.nextInt();

        Salas sala01 = new Salas(capacidadSala, nombreSala);

        System.out.print("Película: ");
        String nombrePelicula = sc.next();

        sala01.setPelicula(nombrePelicula);



        while (seguir == 1) {

            System.out.println("ESPECTADOR");

            System.out.print("Nombre: ");
            String nombre = sc.next();

            System.out.print("Edad: ");
            int edad = sc.nextInt();

            System.out.print("Fila: ");
            String fila = sc.next();

            System.out.print("Silla: ");
            int silla = sc.nextInt();

            System.out.println("Desea cargar otro espectador? 1 (si); 2 (no)");
            seguir = sc.nextInt();

            espectadores.add(new Espectadores(nombre, edad, fila, silla));

        }

        sala01.setEspectadores(espectadores);


        sala01.getEspectadores();


        System.out.println("ACOMODADOR: ");

        System.out.print("Nombre: ");
        String nombreAcomodador = sc.next();

        System.out.print("Edad: ");
        int edadAcomodador = sc.nextInt();

        Acomodadores acomodador01 = new Acomodadores(nombreAcomodador, edadAcomodador);

        acomodador01.setSala(sala01);


        System.out.print("Sueldo: ");
        double sueldoAcomodador = sc.nextDouble();
        acomodador01.setSueldo(sueldoAcomodador);


        System.out.println(acomodador01.toString());

        Empleados empleado01 = new Empleados("Pepe", 25);

        System.out.println(empleado01.toString());


    }
}
