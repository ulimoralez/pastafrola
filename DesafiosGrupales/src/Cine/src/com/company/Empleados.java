package com.company;

public class Empleados {
    private double sueldo;


    public Empleados(String nombre, int edad) {
        super.setNombre(nombre);
        super.setEdad(edad);
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    //

    @Override
    public String toString() {
        return "Nombre: "+super.getNombre()+"\n"+
                "Edad: "+super.getEdad()+"\n"+
                "Tipo:"+this.getTipo()+"\n"+
                "Sueldo: $"+this.sueldo+"\n"+
    }

    @Override
    public String getTipo() {
        return "Empleado";
    }


}
