package com.company;

public class Acomodadores extends Empleados  implements ParaAcomodadores{
    private Salas sala;

    public Acomodadores (String nombre, int edad){
    super(nombre, edad);
    }


    public String getTipo() {
        return "Acomodador";

    }

    @Override
    public Salas getSala() {
        return this.sala;
    }

    @Override
    public void setSala(Salas sala) {
        this.sala = sala;
    }
}
