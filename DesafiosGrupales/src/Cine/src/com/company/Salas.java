package com.company;
import java.util.TreeSet;
import java.util.Set;

public  class Salas {
    private int capacidad;
    private String pelicula;
    private String nombre;
    private Set<Espectadores> espectadores;

    public Salas(int capacidad, String nombre) {
        this.capacidad = capacidad;
        this.nombre = nombre;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public void setEspectadores(Set<Espectadores> espectadores) {

        if(espectadores.size()>this.capacidad) {
            System.out.println("SALA LLENA");
        }else {
            this.espectadores = espectadores;
        }

    }

    public void getEspectadores() {

        for (Espectadores espectador : espectadores) {
            System.out.println(espectador.toString());
        }

    }

    @Override
    public String toString() {
        return "Salas [capacidad=" + capacidad + ", pelicula=" + pelicula + ", nombre=" + nombre;
    }

}


