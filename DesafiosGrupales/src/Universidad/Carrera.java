package Universidad;

import java.util.*;

public class Carrera implements Informacion{
    private String nombre;
    private   Set<Materia> materias = new HashSet<Materia>();

    public Carrera(String nombre) {
    }

    public void agregarMateria(String nombre, Profesor titular) {

        this.materias.add(new Materia(nombre, titular));
    }

    public void eliminarMateria(String nombre) {

        Iterator<Materia> eliminar = materias.iterator();

        while (eliminar.hasNext()) {
            if (eliminar.next().getNombre().equals(nombre)) {
                eliminar.remove();
                System.out.println(nombre + " fue eliminada");
            }
        }
    }

    public Materia encontrarMateria(String nombreMateria) {
        for (Materia materia : materias) {
            if(materia.getNombre().equals(nombreMateria)) {
                return materia;
            }
        }
        return null;
    }

    public void eliminarEstudiante(int nroLegajo) {
        for (Materia materia : materias) {
            materia.eliminarEstudiante(nroLegajo);
        }
    }

    /*
     * IMPLEMENTACIN DE LA INTERFACE
     */

    @Override
    public int verCantidad() {
        // TODO Auto-generated method stub
        return this.materias.size();
    }


    @Override
    public String listarContenidos() {
        return materias.toString();
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Materia> getMaterias() {
        return materias;
    }

    public void setMaterias(Set<Materia> materias) {
        this.materias = materias;
    }

    @Override
    public String toString() {
        return "\n \n \t Carrera [nomCar=" + nombre + ", materias=" + materias + "]";
    }


}