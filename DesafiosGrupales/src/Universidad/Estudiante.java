package Universidad;

class Estudiante extends Personas{

    public Estudiante(String nombre, String apellido, int legajo){
        super.setNombre(nombre);
        super.setApellido(apellido);
        super.setLegajo(legajo);
    }

    @Override
    public void modificarDatos(String nuevonombre, String nuevoapellido, int nuevolegajo) {

    }


    @Override
    public void modificarDatos() {

    }

    @Override
    public String toString() {
        return "\n \t\t\t\t Estudiante [Nombre()=" + getNombre() + ", Apellido()=" + getApellido() + ", Legajo()="
                + getLegajo() + "\n ]";
    }



}
