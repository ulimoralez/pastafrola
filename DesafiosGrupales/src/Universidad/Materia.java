package Universidad;

import java.util.*;

class Materia implements Informacion{

private String nombre;
private Profesor titular;
private Set<Estudiante> estudiantes = new HashSet<Estudiante>();



public Materia(String nombre, Profesor titular) {
        this.nombre = nombre;
        this.titular = titular;
        }

public void agregarEstudiante(String nombre, String apellido, int legajo){
        estudiantes.add(new Estudiante(nombre, apellido, legajo));
        }

public void eliminarEstudiante(int nroLegajo){

        Iterator<Estudiante> iterador = estudiantes.iterator();
        while (iterador.hasNext()) {
        int elementoEliminar = iterador.next().getLegajo();
        if (elementoEliminar == nroLegajo)
        iterador.remove();
        }

        }

public void modificarTitular(Profesor titular){
        this.titular = titular;
        System.out.println("Titular modificado");
        }

/*
 * GENTERS AND SETERS
 */

@Override
public int verCantidad() {
        // TODO Auto-generated method stub
        return this.estudiantes.size();
        }

public String getNombre() {
        return nombre;
        }

public void setNombre(String nombre) {
        this.nombre = nombre;
        }

public Profesor getTitular() {
        return titular;
        }

public void setTitular(Profesor titular) {
        this.titular = titular;
        }

public Set<Estudiante> getEstudiantes() {
        return estudiantes;
        }

public void setEstudiantes(Set<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
        }


@Override
public String listarContenidos() {
        // TODO Auto-generated method stub
        return this.estudiantes.toString();
        }

@Override
public String toString() {
        return "\n \n \t\t Materia [nomMat=" + nombre + ", titular=" + titular + ", estudiantes=" + estudiantes + "]";
        }
}
