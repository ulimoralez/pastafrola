package Universidad;

public class Cabeza {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        Facultad frr = new Facultad("UTN");

        frr.agregarCarrera("TUP");
        frr.agregarCarrera("SISTEMAS");
        frr.agregarCarrera("ADMINISTRACION");


        Profesor titular = new Profesor(15000, 2, "Facundo", "Uferer", 3553);


        frr.agregarMateriaCarrera("TUP", "Laboratorio de Computación II", titular);

        frr.agregarMateriaCarrera("SISTEMAS", "Laboratorio 2", titular);

        frr.agregarMateriaCarrera("SISTEMAS", "Laboratorio 1", titular);

        frr.eliminarMateriaDeCarrera("SISTEMAS", "Laboratorio 2");



        frr.encontrarMateriaEnCarrera("TUP", "Laboratorio de Computación II");



        Estudiante estudiante = new Estudiante("Elba", "Jonazo", 21256);
        Estudiante estudiante2 = new Estudiante("Helen", "Chufe", 32569);

        frr.agregarEstudiante("TUP", "Laboratorio  2", estudiante);
        frr.agregarEstudiante("TUP", "Laboratorio 2", estudiante2);



        frr.eliminarEstudiante("TUP", "Laboratorio 2", 2548);



        Profesor nuevoProvesor = new Profesor(20000, 3, "Don", "Pepe", 02564);

        frr.modificarTitular("TUP", "Laboratorio de Computación II", nuevoProvesor);

        System.out.println(frr.toString());
    }
}
